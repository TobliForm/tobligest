# TobliGest

TobliGest est une application web de gestion d'appartement ou d'hôtel.

## Environnement de développement

### Pré-requis

* PHP 7.4 
* Composer
* Symfony CLI

### Vous pouvez verifier les pré-requis avec la commande suivante (de la CLI Symfony) :

```bash
symfony check:requirements
```

### Lancer l'environnement de développement

```bash
symfony serve -d
```